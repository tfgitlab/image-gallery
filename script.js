const imgs = document.getElementById('imgs')
const leftBtn = document.getElementById('left')
const rightBtn = document.getElementById('right')
const img = document.querySelectorAll('#imgs img')

let idx = 0

let interval = setInterval(run, 2000)

function run() {
    idx++
    changeImage()
}

function changeImage() {
    
    if (idx > img.length-1) {
        idx=0
    } else if(idx <0) {
        idx = img.length-1
    }
    // console.log("idx: "+ idx)

    imgs.style.transform = `translateX(${-idx * 500}px)`
}

rightBtn.addEventListener('click', () => {
    // console.log("RB clicked")
    idx++
    changeImage()
    resetInterval()
})

leftBtn.addEventListener('click', () => {
    // console.log("LB clicked")
    idx--
    changeImage() 
    resetInterval()
})

function resetInterval() {
    clearInterval(interval)
    interval = setInterval(run, 2000)
}